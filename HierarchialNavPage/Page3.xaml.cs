﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace HierarchialNavPage
{
    public partial class Page3 : ContentPage
    {
        public Page3(string str)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Page3)}:  ctor");
            InitializeComponent();
            word3Label.Text = str;

        }
    }
}
