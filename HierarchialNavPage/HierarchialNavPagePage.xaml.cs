﻿using Xamarin.Forms;
using System.Diagnostics;
using System;

namespace HierarchialNavPage
{
    public partial class HierarchialNavPagePage : ContentPage
    {
        public HierarchialNavPagePage()
        {
            Debug.WriteLine("HierarchialNavPagePage");
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this,false);
        }

        async void Page2(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Page2)}");

            bool usersResponse = await DisplayAlert("Page2!",
                         "Are you really sure you want to go to Page 2??",
                         "Yes!",
                         "No");

            if (usersResponse == true)
            {
                await Navigation.PushAsync(new Page2("You made it to Page 2!!!"));
            }
          
        }

        async void Page3(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Page3)}");
            bool usersResponse = await DisplayAlert("Page3!",
                         "Are you really sure you want to go to Page 3??",
                         "Yes!",
                         "No");

            if (usersResponse == true)
            {
                await Navigation.PushAsync(new Page3("You made it to Page 3!!!"));
            }
          

        }



    }
}
