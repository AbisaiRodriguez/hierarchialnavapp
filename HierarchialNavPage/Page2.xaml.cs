﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace HierarchialNavPage
{
    public partial class Page2 : ContentPage
    {
        public Page2(string word)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Page2)}:  ctor");
            InitializeComponent();

            wordLabel.Text = word;

        }
    }
}